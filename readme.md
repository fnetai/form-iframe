# @fnet/form-iframe

The @fnet/form-iframe project provides a straightforward solution for embedding content into a web page using an iframe within a React component. It aims to simplify the integration of external content, such as videos or interactive forms, directly into your layout.

## How It Works

This project uses a React component that contains an iframe, allowing you to seamlessly incorporate and display content from another source. By simply passing a URL as a prop, you can render the desired content within the borders of your existing layout.

## Key Features

- **Responsive Embedding**: The iframe seamlessly adapts to the full width and height of its container, ensuring content is displayed effectively on various screen sizes.
- **Simplified Integration**: By leveraging a single React component, you can easily include external content without needing additional setup.
- **Rich Media Support**: The iframe supports a range of permissions to enable functionalities such as autoplay and clipboard access.

## Conclusion

@fnet/form-iframe offers a straightforward way to embed external content within your React application. Its simple structure makes it easy to display rich media or interactive forms, enhancing the functionality and user experience of your pages without unnecessary complexity.