import React from 'react';

import Form from '../src';

export default () => {
  return (
    <Form
      title={"Form"}
      input={{ src: "https://www.youtube.com/embed/3sN-DKevBuE?si=umsthUda9nlsoKn3" }}
      contentFlexGrow={1}
      controls={[
        { title: "Control 1", action: () => alert('Control 1') },
        { title: "Control 2", action: () => alert('Control 2') },
      ]}
    ></Form>
  )
}