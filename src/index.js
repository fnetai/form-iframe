import React from "react";
import Layout from "@fnet/react-layout-asya";

export default ({ input, ...props } = {}) => {

  const form = props?.form || {};

  const [src, setSrc] = React.useState(input?.src);

  React.useEffect(() => {
    form.setInputSrc = setSrc;
  }, []);

  return (
    <Layout {...props}>
      <iframe
        style={{ width: "100%", height: "100%", border: "none" }}
        src={src}
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        allowFullScreen
      ></iframe>
    </Layout>
  );
};